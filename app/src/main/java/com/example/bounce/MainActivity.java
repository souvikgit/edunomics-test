package com.example.bounce;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    private SeekBar seekBar;
    private EditText heightTv, massTv;
    private GraphView graphView;
    private Button generate;
    double height = 0, coefficient = 0;
    private String TAG = MainActivity.class.getSimpleName();
    ArrayList<Bounce> list;
    private final double g = 9.8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        heightTv = findViewById(R.id.height);
        massTv = findViewById(R.id.mass);
        seekBar = findViewById(R.id.coff);
        graphView = findViewById(R.id.graph);
        generate = findViewById(R.id.generate_graph);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                coefficient = Double.valueOf(progress) / 10;
                Log.d(TAG, "Cofficient value: " + coefficient);
                Toast.makeText(MainActivity.this, "" + coefficient, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(heightTv.getText())) {
                    heightTv.setError("Field cannot be empty");
                }
                if (TextUtils.isEmpty(massTv.getText())) {
                    massTv.setError("Field cannot be empty");
                }
                if (!TextUtils.isEmpty(massTv.getText()) && !TextUtils.isEmpty(heightTv.getText())) {
                    calculateBounce();
                }
            }
        });

    }

    private void calculateBounce() {
        double u = 0, v = 0, t = 0, m = 0, E = 0;
        int count = 0;
        m = Double.valueOf(massTv.getText().toString());
        list = new ArrayList();
        height = Double.valueOf(heightTv.getText().toString());
        Bounce obj = new Bounce(0, height);
        list.add(obj);
        while (height >= 0.001) {
            count++;
            v = Math.pow(u + 2 * g * height, 0.5);
            t += (v - u) / g;
            E = 0.5 * m * v * v;
            if (count % 2 == 0) {
                obj = new Bounce(t, height);
            } else {
                obj = new Bounce(t, 0);
                E = E - E * coefficient;
            }
            list.add(obj);
            height = E / (m * g);
        }
        drawGraph();
    }

    private void drawGraph() {
        graphView.removeAllSeries();
        ArrayList<DataPoint> dlist = new ArrayList<>();
        String[] vertivalLable = new String[list.size()];
        String[] horizontalLable = new String[list.size()];
        int j = 0, k = 0;
        for (Bounce i : list) {
            dlist.add(new DataPoint(i.getTime(), i.getHeight()));
        }
        DataPoint[] arr = new DataPoint[dlist.size()];
        for (int i = 0; i < dlist.size(); i++) {
            arr[i] = dlist.get(i);
        }
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(arr);
        graphView.getViewport().setScalable(true);
        graphView.getGridLabelRenderer().setHorizontalAxisTitle("Time in seconds");
        graphView.getGridLabelRenderer().setVerticalAxisTitle("Height in meters");
        graphView.getGridLabelRenderer().setHighlightZeroLines(true);
        graphView.getViewport().setMinX(0);
        graphView.getViewport().setMaxX(arr[dlist.size()-1].getX());
        graphView.getViewport().setMinY(0);
        graphView.getViewport().setMaxY(arr[dlist.size()-1].getY());
        graphView.addSeries(series);
    }
}
