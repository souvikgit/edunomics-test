package com.example.bounce;

public class Bounce {
    double time,height;

    public Bounce(double time, double height) {
        this.time = time;
        this.height = height;
    }

    public Bounce() {
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}
